var postcss = require('gulp-postcss')
var cleanCSS = require('gulp-clean-css')
var gulp = require('gulp')

const paths = {
  css: 'assets/css/*.css',
  dest: './dist'
}

gulp.task('css', () => {
  return gulp.src(paths.css)
    .pipe(postcss())
    .pipe(cleanCSS({}))
    .pipe(gulp.dest(paths.dest))
})

gulp.task('watch', () => {
  gulp.watch(paths.css, ['css'])
})

gulp.task('default', ['watch'])

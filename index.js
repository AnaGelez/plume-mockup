const Koa = require('koa')
const views = require('koa-views')
const serve = require('koa-static')
const path = require('path')
const fs = require('fs')

const app = new Koa()

app.use(views(path.join(__dirname, 'views'), {
  map: {
    html: 'atpl'
  }
}))

app.use(serve('dist'))

app.use(async ctx => {
  const data = JSON.parse(fs.readFileSync('data.json'))

  if (ctx.url === '/') {
    await ctx.render('home', {
      latest: data.articles,
      title: 'Home'
    })
  } else if (ctx.url === '/write') {
    await ctx.render('write', {
      title: 'New story'
    })
  }
})

console.log('Server running on http://localhost:3000')
app.listen(3000)
